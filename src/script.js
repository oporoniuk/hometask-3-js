'use strict';

// Теоретичні питання
// 1. логічний оператор - ! (не), && (і), || (або)
// логічні оператори допомагаюь нам обєднувати прості умови у складний логічний вираз,
// щоб  можна було побудувати складну умови, коли це потрібно.
// ! - означає не, !! - повертає до пояаткового значення.
// && - коли використовуємо цей оператор, то це оначає, що всі умови повинні бути true, тоді результат буде  true.
// || -  коли використовуємо цей оператор, то це оначає, що одна з умов повинна бути true, тоді результат буде true.






// Практичні завдання.
//1
let userAge = prompt("What is your age?");

while(isNaN(userAge)) {
    console.log(typeof userAge, isNaN(userAge));
    userAge = Number(prompt ("Please enter your age with a number!"));
}
console.log(userAge);

if (userAge < 12) {
    alert ("You are a child");
} else if (userAge < 18) {
    alert ("You are a teenager");
} else {
    alert ("you are an adult");
}
//2
let month = prompt ("Який місяць вас цікавить?");

switch (month.toLowerCase()) {
        case "січень":
            case "березень":
                case "травень":
                    case "липень":
                        case "серпень":
                            case "жовтень":
                                case "грудень":
                                    console.log("У місяці 31 день");
                                    break;
        case "квітень":
            case "червень":
                case "вересень":
                    case "листопад":
                        console.log("У місяці 30 днів");
                        break;
        case "лютий":
            console.log("У місяці 28 днів");
            break;

        default:
            console.log("Значення місяця невірне");
}
